var campos = ['nome', 'email', 'telefone', 'mensagem'];

$(document).ready(function () {


    $("#submitContato").click(function () {
        setFormValues();
    });

    $("#submitSalvar").click(function () {
        setFormValues();
        getFormValues();
    });
    
    
    $('#contactForm')
    .form({
        on: 'blur',
        fields: {
            nome: {
                identifier  : 'nome',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Este campo é obrigatório.'
                    }
                ]
            },
            email: {
                identifier  : 'email',
                rules: [
                    {
                        type   : 'email',
                        prompt : 'Digite um email válido.'
                    }
                ]
            },
            telefone: {
                identifier: 'telefone',
                rules: [
                    {
                        type: 'regExp[/^(?:(?:\\+|00)?(55)\\s?)?(?:\\(?([1-9][0-9])\\)?\\s?)?(?:((?:9\\d|[2-9])\\d{3})\\-?(\\d{4}))$/]',
                        prompt: 'Digite um telefone válido.'
                    }
                ]
            },
            mensagem: {
                identifier  : 'mensagem',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Este campo é obrigatório.'
                    }
                ]
            }
        }
    });
    

    $.each(campos, function( index, value ) {
        $( "#" + value ).keyup(function() {
            $( ".output-" + value ).text($(this).val());
        }).keyup();
    });
    
    $(".telefone").mask('(00) 0000-0000');

    if (window.location.pathname === "/hotsite/frontend-edit") {
        getFormValues();
    }   
    
});

function setFormValues() {
    var
    $form = $('#contactForm'),
    allFields = $form.form('get values');
    ;

    $.each(allFields, function( index, value ) {
        sessionStorage.setItem(index, value);
    });  

    return false;
}

function getFormValues() {
    $.each(campos, function( index, value ) {
         $('#' + value).val(sessionStorage.getItem(value));
    });    
}  